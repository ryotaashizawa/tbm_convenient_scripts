import sys
import json

src_json_file, dst_json_file, sge_task_id = sys.argv[1:]

with open(src_json_file) as f:
    af2_task_dicts = json.load(f)

af2_task_dict = af2_task_dicts[int(sge_task_id) - 1]

with open(dst_json_file, 'w') as f:
    json.dump(af2_task_dict, f, indent=4)
