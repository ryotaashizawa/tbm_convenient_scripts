#!/bin/bash -l
#$ -l h_rt=12:00:00
#$ -N TBM_run
#$ -m n
#$ -j y
#$ -P k3domics
#$ -t 1-204
#$ -cwd
#$ -o ./logs/
#$ -e ./logs/
#$ -l gpus=1
#$ -l gpu_c=3.5

# change here according to the project
export OUTPUT_DIR=/projectnb/sc3dm/rashizaw/09_ClusproTBM/21_AMPA/TBM_results/spliting
export JOBS_JSON_PATH=/projectnb/sc3dm/rashizaw/09_ClusproTBM/21_AMPA/arrayjob_splitdomain_input.json

# independen on project
export PATH="/projectnb/sc3dm/rashizaw/.conda/envs/for_TBM/bin/:$PATH"
mkdir logs
export RUN_TBM_PATH=/projectnb/sc3dm/rashizaw/09_ClusproTBM/templatebased_scc_scripts/run_template.sh
cwd=$(pwd)

# loop for task_id
mkdir $OUTPUT_DIR/$SGE_TASK_ID
python create_input_json_for_each_arrayjob.py $JOBS_JSON_PATH $OUTPUT_DIR/$SGE_TASK_ID/input.json $SGE_TASK_ID
cd $OUTPUT_DIR/$SGE_TASK_ID
python $cwd/run_tbm.py input.json $RUN_TBM_PATH
bash run_tbm.bash