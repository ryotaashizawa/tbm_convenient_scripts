import sys
import json
import subprocess
import os
from pathlib import Path

cwd = os.getcwd()
input_json, tbm_run_template_sh_path = sys.argv[1:]

with open(input_json, "r") as f:
    input_dict = json.load(f)

opt_args = str(input_dict["optional arg"])
stoichiometry = str(input_dict["stoichiometry"])
hhrpath1, hhrpath2 = input_dict["hhr files path"]
fastapath1, fastapath2 = input_dict["fasta files path"]
cmd = f"bash {tbm_run_template_sh_path} {opt_args} {stoichiometry} {hhrpath1} {hhrpath2} {fastapath1} {fastapath2}"
print(cmd)

with open(Path(cwd)/"run_tbm.bash", "w") as f:
    f.write(cmd)
